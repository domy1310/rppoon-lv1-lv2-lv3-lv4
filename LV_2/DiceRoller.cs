﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2
{
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        // private ILogger logger; Zadatak 4

        /* Zadatak 4
        public DiceRoller(ILogger logger)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.logger = logger; 
        }*/

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            this.dice.Add(die);
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }

        /* Zadatak 4
        public void LogRollingResults()
        {
            foreach (int rollingResult in this.resultForEachRoll)
            {
                logger.Log(rollingResult.ToString());
            }
        }*/

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (int rollingResult in this.resultForEachRoll)
            {
                stringBuilder.Append(rollingResult).Append("\n");
            }

            return stringBuilder.ToString();
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }
    }
}
