﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2
{
    class ConsoleLogger : ILogger
    {
        public void Log(ILogable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }

        /* Zadatak 4
        public void Log(string message)
        {
            Console.WriteLine(message);
        }*/
    }
}
