﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV_2
{
    class FlexibleDiceRoller : IFlexibleDiceRoller, ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public List<Die> Dice
        {
            get { return this.dice; }
        }

        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            this.dice.Add(die);
        }

        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }

        public int RemoveDice(int numberOfSides)
        {
            return dice.RemoveAll(die => die.NumberOfSides == numberOfSides);
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (Die die in this.dice)
            {
                stringBuilder.Append(die.NumberOfSides).Append(" ");
            }

            return stringBuilder.ToString();
        }
    }
}
