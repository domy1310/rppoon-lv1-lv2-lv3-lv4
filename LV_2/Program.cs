﻿using System;
using System.Collections.Generic;

namespace LV_2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ===== Zadatak 1 ===== */
            /*DiceRoller diceRoller = new DiceRoller();
            for(int i = 0; i < 20; i++)
            {
                Die die = new Die(6);
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            PrintRollingResults(diceRoller.GetRollingResults());*/

            /* ===== Zadatak 2 ===== */
            /*Random randomGenerator = new Random();

            DiceRoller diceRoller = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                Die die = new Die(6, randomGenerator);
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            PrintRollingResults(diceRoller.GetRollingResults());*/

            /* ===== Zadatak 3 ===== */
            /*DiceRoller diceRoller = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                Die die = new Die(6);
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            PrintRollingResults(diceRoller.GetRollingResults());*/

            /* ===== Zadatak 4 ===== */
            /*ConsoleLogger consoleLogger = new ConsoleLogger();
            DiceRoller diceRoller = new DiceRoller(consoleLogger);
            for (int i = 0; i < 20; i++)
            {
                Die die = new Die(6);
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            diceRoller.LogRollingResults();*/

            /* ===== Zadatak 5 ===== */
            DiceRoller diceRoller = new DiceRoller();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            FileLogger fileLogger = new FileLogger("zad5.csv");

            for (int i = 0; i < 20; i++)
            {
                Die die = new Die(6);
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            Console.WriteLine("Rezultati nakon bacanja kockica: ");
            consoleLogger.Log(diceRoller);
            fileLogger.Log(diceRoller);

            /* ===== Zadatak 7 ===== */
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            RandomGenerator randomGenerator = RandomGenerator.GetInstance();
            for (int i = 0; i < 20; i++)
            {
                Die die = new Die(randomGenerator.NextInt(1, 6));
                flexibleDiceRoller.InsertDie(die);
            }

            List<Die> dice = flexibleDiceRoller.Dice;
            Console.WriteLine("Lista kockica sa sljedecim brojem stranica: ");
            consoleLogger.Log(flexibleDiceRoller);

            Console.WriteLine("\nLista nakon brisanja kockica s brojem stranica 2: ");
            Console.WriteLine("Broj obrisanih kockica: " + flexibleDiceRoller.RemoveDice(2));
            consoleLogger.Log(flexibleDiceRoller);

            Console.WriteLine("\nLista nakon brisanja kockica s brojem stranica 3: ");
            Console.WriteLine("Broj obrisanih kockica: " + flexibleDiceRoller.RemoveDice(3));
            consoleLogger.Log(flexibleDiceRoller);

            Console.WriteLine("\nLista nakon brisanja kockica s brojem stranica 4: ");
            Console.WriteLine("Broj obrisanih kockica: " + flexibleDiceRoller.RemoveDice(4));
            consoleLogger.Log(flexibleDiceRoller);


        }

        public static void PrintRollingResults(IList<int> rollingResults)
        {
            foreach (int result in rollingResults)
            {
                Console.WriteLine(result);
            }
        }

    }
}
