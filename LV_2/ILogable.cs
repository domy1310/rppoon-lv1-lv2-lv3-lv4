﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2
{
    interface ILogable
    {
        public string GetStringRepresentation();
    }
}
