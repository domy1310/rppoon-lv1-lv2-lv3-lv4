﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;

        /* Zadatak 1
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }*/

        /* Zadatak 2
        public Die(int numberOfSides, Random randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }*/

        /* Zadatak 3 */
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }

        public int NumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }
}
