﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_2
{
    interface IFlexibleDiceRoller
    {
        public void InsertDie(Die die);
        public void RemoveAllDice();
        public void RollAllDice();
    }
}
