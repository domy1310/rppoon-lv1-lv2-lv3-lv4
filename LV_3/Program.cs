﻿using System;
using System.Collections.Generic;
using System.Data;

namespace LV_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ===== Zadatak 1 ===== */
            Dataset dataset = new Dataset("zad1.csv");
            Console.WriteLine("Lista 1:");
            PrintDataset(dataset.GetData());

            Dataset data = (Dataset)dataset.Clone();
            Console.WriteLine("Kopija liste:");
            PrintDataset(data.GetData());

            Console.WriteLine("Lista 1 nakon brisanja:");
            dataset.ClearData();

            Console.WriteLine("Kopija liste:");
            PrintDataset(data.GetData());

            /* ===== Zadatak 2 ===== */
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double[,] matrix = matrixGenerator.GenerateMatrix(2, 3);
            PrintMatrix(matrix, 2, 3);

            /* ===== Zadatak 3 ===== */
            Logger logger = Logger.GetInstance();
            logger.Log("Bokkkkkkkkk");

            /* ===== Zadatak 4 ===== */
            ConsoleNotification consoleNotification = new ConsoleNotification("Dominik", "RPPOON", "Poruka", DateTime.Now, Category.INFO, ConsoleColor.Cyan);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(consoleNotification);

            /* ===== Zadatak 5 ===== */
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationManager.Display(notificationBuilder
                .SetAuthor("Dominik Tkalčec")
                .SetLevel(Category.ERROR)
                .SetText("Greška!")
                .Build());

            /* ===== Zadatak 6 ===== */
            Director director = new Director();
            director.ErrorNotification(notificationBuilder, "Ivan");
            notificationManager.Display(notificationBuilder.Build());

            director.AlertNotification(notificationBuilder, "Pero");
            notificationManager.Display(notificationBuilder.Build());

            director.InfoNotification(notificationBuilder, "Djuro");
            notificationManager.Display(notificationBuilder.Build());

            /* ===== Zadatak 7 ===== */
            NotificationManager clonedNotification = new NotificationManager();
            Prototype prototype = notificationBuilder.Build().Clone();
            clonedNotification.Display((ConsoleNotification)prototype);
            // U ovom slučaju nema razlike između plitkog i dubokog kopiranja budući da smo klasu ConsoleNotification 
            // pozivali preko Directora tako da su parametri unaprijed određeni te ih nije moguće mijenjati.
        }

        private static void PrintMatrix(double[,] matrix, int row, int column)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private static void PrintDataset(IList<List<string>> data)
        {
            foreach (List<string> row in data)
            {
                foreach (string element in row)
                {
                    Console.Write(element);
                }
                Console.WriteLine();
            }
        }
    }
}
