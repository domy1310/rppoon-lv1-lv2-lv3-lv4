﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3
{
    class Dataset : Prototype
    {
        private List<List<string>> data;

        public Dataset() 
        {
            this.data = new List<List<string>>();
        }

        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public Dataset(List<List<string>> data)
        {
            this.data = data;
        }

        private void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<String> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }

        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }

        public void ClearData()
        {
            this.data.Clear();
        }

        public Prototype Clone()
        {
            Dataset clone = (Dataset)this.MemberwiseClone();
            clone.data = new List<List<string>>(this.data.Count);
            foreach (List<string> item in this.data)
            {
                clone.data.Add(item);
            }

            /* 2. način
            List<List<string>> copyOfData = new List<List<string>>();
            foreach (List<string> item in this.data)
            {
                copyOfData.Add(item);
            }
            return (Prototype)new Dataset(copyOfData);*/
            return clone;
        }
    }
}
