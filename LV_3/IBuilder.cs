﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace LV_3
{
    interface IBuilder
    {
        ConsoleNotification Build();
        IBuilder SetAuthor(string author);
        IBuilder SetTitle(string title);
        IBuilder SetTime(DateTime time);
        IBuilder SetLevel(Category level);
        IBuilder SetColor(ConsoleColor color);
        IBuilder SetText(string text);

    }
}
