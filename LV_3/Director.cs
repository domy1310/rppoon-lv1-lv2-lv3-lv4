﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3
{
    class Director
    {
        public void ErrorNotification(IBuilder builder, string author)
        {
            builder.SetAuthor(author)
                .SetColor(ConsoleColor.Red)
                .SetLevel(Category.ERROR)
                .SetTitle("Greška")
                .SetText("Došlo je do greške.")
                .SetTime(DateTime.Now);
        }

        public void InfoNotification(IBuilder builder, string author)
        {
            builder.SetAuthor(author)
                .SetColor(ConsoleColor.DarkYellow)
                .SetLevel(Category.INFO)
                .SetTitle("Info")
                .SetText("Ovo je info.")
                .SetTime(DateTime.Now);
        }

        public void AlertNotification(IBuilder builder, string author)
        {
            builder.SetAuthor(author)
                .SetColor(ConsoleColor.DarkGreen)
                .SetLevel(Category.ALERT)
                .SetTitle("Upozorenje")
                .SetText("Ovo je upozorenje.")
                .SetTime(DateTime.Now);
        }
    }
}
