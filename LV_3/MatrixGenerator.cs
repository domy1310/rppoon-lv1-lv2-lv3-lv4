﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private double[,] matrix;
        
        private MatrixGenerator()
        {
            this.matrix = new double[0,0];
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public double[,] GenerateMatrix(int row, int column)
        {
            this.matrix = new double[row, column];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    this.matrix[i, j] = this.GenerateDoubleNumber();
                }
            }
            return this.matrix;
        }

        public double GenerateDoubleNumber()
        {
            Random randomGenerator = new Random();
            return randomGenerator.NextDouble();
        }
    }
}
