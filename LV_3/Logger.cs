﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LV_3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

        public string FilePath
        {
            get { return this.filePath; }
            set { this.filePath = value; }
        }

        private Logger()
        {
            this.filePath = "zad_3.csv";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string data)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(data);
            }
        }
    }
}
