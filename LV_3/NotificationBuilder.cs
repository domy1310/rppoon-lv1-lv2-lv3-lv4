﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3
{
    class NotificationBuilder : IBuilder
    {
        private string author = string.Empty;
        private ConsoleColor color = ConsoleColor.White;
        private Category level = Category.INFO;
        private string text = string.Empty;
        private DateTime time = DateTime.Now;
        private string title = string.Empty;

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, time,level, color);
        }

        public IBuilder SetAuthor(string author)
        {
            this.author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.time = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.title = title;
            return this;
        }
    }
}
