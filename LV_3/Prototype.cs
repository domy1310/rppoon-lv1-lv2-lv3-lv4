﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3
{
    interface Prototype
    {
        Prototype Clone();
    }
}
