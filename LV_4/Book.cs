﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class Book : IRentable
    {
        private readonly double BaseBookPrice = 3.99;

        public Book(string title)
        {
            this.Title = title;
        }

        public string Title
        {
            get;
            private set;
        }

        public string Description
        {
            get { return this.Title; }
        }

        public double CalculatePrice()
        {
            return BaseBookPrice;
        }
    }
}
