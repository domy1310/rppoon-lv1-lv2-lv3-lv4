﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    abstract class RentableDecorater : IRentable
    {
        private IRentable rentable;

        public RentableDecorater(IRentable rentable)
        {
            this.rentable = rentable;
        }

        public virtual double CalculatePrice()
        {
            return rentable.CalculatePrice();
        }

        public virtual String Description
        {
            get { return rentable.Description; }
        }
    }
}
