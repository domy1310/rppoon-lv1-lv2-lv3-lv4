﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace LV_4
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
