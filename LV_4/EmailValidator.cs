﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class EmailValidator : IEmailValidatorService
    {
        public int MinLength
        {
            get;
            private set;
        }

        public EmailValidator(int minLength)
        {
            this.MinLength = minLength;
        }
        
        public bool IsValidAddress(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return IsLongEnough(candidate) && IsContainsAt(candidate) && IsContainsDotCom(candidate);
        }

        private bool IsContainsDotCom(string candidate)
        {
            string lastFourChars = candidate.Substring(candidate.Length - 4);
            if (lastFourChars.Equals(".com"))
                return true;
            return false;
        }

        private bool IsContainsAt(string candidate)
        {
            if (candidate.Contains("@"))
                return true;
            return false;
        }

        private bool IsLongEnough(string candidate)
        {
            return candidate.Length >= this.MinLength;
        }
    }
}
