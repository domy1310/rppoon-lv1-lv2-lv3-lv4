﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
