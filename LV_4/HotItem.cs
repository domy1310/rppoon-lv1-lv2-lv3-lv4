﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class HotItem : RentableDecorater
    {
        private readonly double HotItemBonus = 1.99;

        public HotItem(IRentable rentable) : base(rentable) { }

        public override string Description
        {
            get { return "Trending " + base.Description;  }
        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() + this.HotItemBonus;
        }
    }
}
