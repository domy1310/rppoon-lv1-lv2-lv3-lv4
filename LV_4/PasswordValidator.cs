﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class PasswordValidator : IPasswordValidatorService
    {
        public int MinLength
        {
            get;
            private set;
        }

        public PasswordValidator(int minLength)
        {
            this.MinLength = minLength;
        }

        public bool IsValidPassword(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return IsLongEnough(candidate) && ContainsDifferentCharacterTypes(candidate);
        }

        private bool ContainsDifferentCharacterTypes(string candidate)
        {
            bool hasLower = false, hasUpper = false, hasNumber = false;
            foreach (char c in candidate)
            {
                if (char.IsDigit(c)) hasNumber = true;
                if (char.IsLower(c)) hasLower = true;
                if (char.IsUpper(c)) hasUpper = true;
            }
            return (hasLower && hasNumber && hasUpper);
        }

        private bool IsLongEnough(string candidate)
        {
            return candidate.Length >= this.MinLength;
        }
    }
}
