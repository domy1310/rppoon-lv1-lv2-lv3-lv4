﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analiyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analiyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            int count = 0;
            double[][] data = new double[dataset.GetData().Count][];
            foreach (List<double> line in dataset.GetData())
            {
                data[count] = line.ToArray();
                count++;
            }
            return data;
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analiyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analiyticsService.PerRowAverage(data);
        }
    }
}
