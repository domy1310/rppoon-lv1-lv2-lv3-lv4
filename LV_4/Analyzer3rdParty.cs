﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV_4
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for(int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }

        public double[] PerColumnAverage(double[][] data)
        {
            return Enumerable.Range(0, data[0].Length).Select(i => data.Average(a => a[i])).ToArray();
        }
    }
}
