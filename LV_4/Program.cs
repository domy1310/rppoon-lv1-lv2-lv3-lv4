﻿using System;
using System.Collections.Generic;

namespace LV_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // ===== Zadatak 1,2 =====
            Dataset dataset = new Dataset("zad1.csv");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);

            Console.WriteLine("Suma po redcima:");
            foreach (double row in adapter.CalculateAveragePerRow(dataset))
            {
                Console.WriteLine(row);
            }
            Console.WriteLine("Suma po stupcima:");
            foreach (double column in adapter.CalculateAveragePerColumn(dataset))
            {
                Console.WriteLine(column);
            }

            // ===== Zadatak 3 =====
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(new Video("Need for speed"));
            rentables.Add(new Book("Harry Potter"));

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            Console.WriteLine("\nIznajmljeno:");
            rentingConsolePrinter.DisplayItems(rentables);
            Console.WriteLine("\nUkupna cijena:");
            rentingConsolePrinter.PrintTotalPrice(rentables);

            // ===== Zadatak 4 =====
            IRentable video = new Video("Fast and forious");
            IRentable book = new Book("American Spy");
            IRentable hotVideo = new HotItem(video);
            IRentable hotBook = new HotItem(book);

            rentables.Add(hotVideo);
            rentables.Add(hotBook);
            Console.WriteLine("\nIznajmljeno:");
            rentingConsolePrinter.DisplayItems(rentables);
            Console.WriteLine("\nUkupna cijena:");
            rentingConsolePrinter.PrintTotalPrice(rentables);

            // ===== Zadatak 5 =====
            List<IRentable> flashSale = new List<IRentable>();

            foreach (IRentable rentable in rentables)
            {
                flashSale.Add(new DiscountedItem(rentable, 0.5));
            }
            Console.WriteLine("\nIznajmljeno:");
            rentingConsolePrinter.DisplayItems(flashSale);
            Console.WriteLine("\nUkupna cijena:");
            rentingConsolePrinter.PrintTotalPrice(flashSale);

            // ===== Zadatak 6 =====
            string emailExample_1 = "dtkalcec.gmail.com";
            string emailExample_2 = "dtkalcec@gmail";
            string emailExample_3 = "dtkalcec@gmail.com";

            string passwordExample_1 = "dD4";
            string passwordExample_2 = "12345678";
            string passwordExample_3 = "dddDDD1";

            PasswordValidator passwordValidator = new PasswordValidator(6);
            Console.WriteLine("Provjera lozinke: " + passwordExample_1 + " => " + passwordValidator.IsValidPassword(passwordExample_1));
            Console.WriteLine("Provjera lozinke: " + passwordExample_2 + " => " + passwordValidator.IsValidPassword(passwordExample_2));
            Console.WriteLine("Provjera lozinke: " + passwordExample_3 + " => " + passwordValidator.IsValidPassword(passwordExample_3));

            EmailValidator emailValidator = new EmailValidator(9);
            Console.WriteLine("Provjera emaila: " + emailExample_1 + " => " + emailValidator.IsValidAddress(emailExample_1));
            Console.WriteLine("Provjera emaila: " + emailExample_2 + " => " + emailValidator.IsValidAddress(emailExample_2));
            Console.WriteLine("Provjera emaila: " + emailExample_3 + " => " + emailValidator.IsValidAddress(emailExample_3));

            // ===== Zadatak 7 =====
            RegistrationValidator registrationValidator = new RegistrationValidator();
            bool isUserEntryValid = false;
            Console.WriteLine("\n\nPrijava korisnika: ");
            do
            {
                isUserEntryValid = registrationValidator.IsUserEntryValid(UserEntry.ReadUserFromConsole());
            } while (isUserEntryValid != true);

        }
    }
}
