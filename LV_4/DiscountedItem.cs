﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace LV_4
{
    class DiscountedItem : RentableDecorater
    {
        public DiscountedItem(IRentable rentable, double discountItemPercent) : base(rentable)
        {
            this.DiscountItemPercent = discountItemPercent;
        }

        public double DiscountItemPercent
        {
            get;
        }

        public override string Description
        {
            get { return "Now at " + (this.DiscountItemPercent * 100) + "% off " + base.Description; }
        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() - (base.CalculatePrice() * DiscountItemPercent);
        }
    }
}
