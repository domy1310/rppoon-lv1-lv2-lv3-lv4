﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    class RegistrationValidator : IRegistrationValidator
    {
        private const int MIN_LENGTH = 6;

        EmailValidator emailValidator;
        PasswordValidator passwordValidator;

        public RegistrationValidator()
        {
            this.emailValidator = new EmailValidator(MIN_LENGTH);
            this.passwordValidator = new PasswordValidator(MIN_LENGTH);
        }

        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password);
        }
    }
}
