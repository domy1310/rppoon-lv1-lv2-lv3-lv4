﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
