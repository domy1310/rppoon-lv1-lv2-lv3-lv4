﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace LV_1
{
    class Note
    {
        private const int MIN_LEVEL = 1;
        private const int MAX_LEVEL = 5;
        private const int DEFAULT_LEVEL = 1;

        private string text;
        private Author author;
        private int level;

        public Note()
        {
            this.text = string.Empty;
            this.author = new Author(string.Empty, string.Empty, string.Empty);
            this.level = DEFAULT_LEVEL;
        }

        public Note(string text, int level)
        {
            this.text = text;
            this.author = new Author("Anonymous", "Anonymous", "anonymous@mail.com");
            this.Level = level;
        }

        public Note(string text, string name, string surname, string email, int level)
        {
            this.text = text;
            this.author = new Author(name, surname, email);
            this.Level = level;
        }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public Author Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }

        public int Level
        {
            get { return this.level; }
            set { 
                if (level >= MIN_LEVEL && level <= MAX_LEVEL)
                    this.level = value;
                else
                    this.level = DEFAULT_LEVEL;
            }
        }

        public override string ToString()
        {
            return "Text: " + text + " Author: " + Author.ToString();
        }

        /*===== Zadatak 1-3 =====*/
        public void SetText(string text)
        {
            this.text = text;
        }
        public string GetText()
        {
            return text;
        }
        public void SetAuthor(Author author)
        {
            this.author = author;
        }
        public Author GetAuthor()
        {
            return author;
        }
        public void SetLevel(int level)
        {
            if (level >= MIN_LEVEL && level <= MAX_LEVEL)
                this.level = level;
            else
                this.level = DEFAULT_LEVEL;
        }
        public int GetLevel()
        {
            return level;
        }
    }
}
