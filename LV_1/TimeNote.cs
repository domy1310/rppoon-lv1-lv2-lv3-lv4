﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    class TimeNote : Note
    {
        private DateTime lastUpdated;

        public TimeNote(String text, string name, string surname, string email, int level) : base(text, name, surname, email, level)
        {
            this.lastUpdated = DateTime.Now;
        }

        public DateTime LastUpdated
        {
            get { return this.lastUpdated; }
            set { this.lastUpdated = DateTime.Now; }
        }

        public override string ToString()
        {
            return base.ToString() + " Last updated: " + lastUpdated.ToString();
        }
    }
}
