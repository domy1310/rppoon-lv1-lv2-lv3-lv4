﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_1
{
    class Author
    {
        private string name;
        private string surname;
        private string email;

        public Author(string name, string surname, string email)
        {
            this.name = name;
            this.surname = surname;
            this.email = email;
        }

        public string Name
        {
            set { this.name = value; }
            get { return this.name; }
        }

        public string Surname
        { 
            get { return this.surname; }
            set { this.surname = value; }
        }

        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

        /*===== Zadatak 1-3 =====*/
        private void SetName(String name)
        {
            this.name = name;
        }
        public string GetName()
        {
            return this.name;
        }
        private void SetSurname(String surname)
        {
            this.surname = surname;
        }
        public string GetSurname()
        {
            return this.surname;
        }
        private void SetEmail(String email)
        {
            this.email = email;
        }
        public string GetEmail()
        {
            return this.email;
        }

        public override string ToString()
        {
            return name + " " + surname + " (" + email + ")";
        }
    }
}
