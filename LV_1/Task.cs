﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV_1
{
    class Task
    {
        private List<Note> tasks;

        public Task()
        {
            tasks = new List<Note>();
        }

        public string GetTasks()
        {
            StringBuilder printTasks = new StringBuilder();
            foreach (Note note in tasks)
            {
                printTasks.Append(note.ToString()).Append("\n");
            }
            tasks.Clear();
            return printTasks.ToString();
        }

        public Note GetTask(int number)
        {
            return tasks.ElementAt(number);
        }

        public void AddTask(Note note)
        { 
            tasks.Add(note);
        }

    }
}
