﻿using System;

namespace LV_1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*===== Zadatak 3 =====*/
            Console.WriteLine("Note 1: ");
            Note note = new Note();
            Console.WriteLine("Text: " + note.GetText() + " Author: " + note.GetAuthor());

            Console.WriteLine("\nNote 2: ");
            note = new Note("Zabilješka broj 2.", 3);
            Console.WriteLine("Text: " + note.GetText() + " Author: " + note.GetAuthor());

            Console.WriteLine("\nNote 3: ");
            note = new Note("Zabilješka broj 3.", "Dominik", "Tkalčec", "dtkalcec1@etfos.hr", 3);
            Console.WriteLine("Text: " + note.GetText() + " Author: " + note.GetAuthor());

            /*===== Zadatak 4-6 =====
            Note note = new Note("Nova zabilješka.", "Dominik", "Tkalčec", "dtkalcec1@etfos.hr", 5);
            Console.WriteLine(note.ToString());

            note = new TimeNote();
            note.Text = "Zabilješka s vremenom.";
            Console.WriteLine(note.ToString());*/

            /*===== Zadatak 7 =====*/
            Task task = new Task();

            Console.WriteLine("Unesite vaše ime: ");
            String name = Console.ReadLine();
            Console.WriteLine("Unesite vaše prezime: ");
            String surname = Console.ReadLine();
            Console.WriteLine("Unesite vašu email adresu: ");
            String email = Console.ReadLine();
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Unesite " + (i + 1) + ". zabilješku: ");
                task.AddTask(new TimeNote(Console.ReadLine(), name, surname, email, 5));
            }

            Console.WriteLine(task.GetTasks());
            Console.WriteLine(task.GetTasks());

        }
    }
}
